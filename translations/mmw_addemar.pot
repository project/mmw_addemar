# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <admin@makemeweb.net>
# Generated from files:
#  mmw_addemar.admin.inc: n/a
#  mmw_addemar.module: n/a
#  mmw_addemar.info: n/a
#  templates/mmw_addemar_registration.tpl.php: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: Fast2Web\n"
"POT-Creation-Date: 2011-10-21 13:37+0000\n"
"PO-Revision-Date: 2011-10-21 16:20+0100\n"
"Last-Translator: MakeMeWeb <admin@makemeweb.net>\n"
"Language-Team: LANGUAGE <admin@makemeweb.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: English\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-Basepath: mmw_addemar\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Poedit-SearchPath-0: en\n"
"X-Poedit-SearchPath-1: fr\n"
"X-Poedit-SearchPath-2: nl\n"
"X-Poedit-SearchPath-3: de\n"

#: mmw_addemar.admin.inc:12
msgid "Addemar token"
msgstr "Addemar token"

#: mmw_addemar.admin.inc:14
msgid "Fedict Addemar application newsletter token"
msgstr "Fedict Addemar application newsletter token"

#: mmw_addemar.module:20
msgid "Addemar subscription form module."
msgstr "Addemar subscription form module."

#: mmw_addemar.module:31
msgid "Newsletter register"
msgstr "Newsletter register"

#: mmw_addemar.module:38
#: mmw_addemar.info:0
msgid "Fast2Web Addemar"
msgstr "Fast2Web Addemar"

#: mmw_addemar.info:0
msgid "Addemar subscription form module"
msgstr "Addemar subscription form module"

#: mmw_addemar.info:0
msgid "Fast2Web"
msgstr "Fast2Web"

#: templates/mmw_addemar_registration.tpl.php:4
msgid "Please enter your email to register to our newsletter"
msgstr "Please enter your email to register to our newsletter"

#: templates/mmw_addemar_registration.tpl.php:7
msgid "email"
msgstr "Your email"

#: templates/mmw_addemar_registration.tpl.php:9
msgid "Register"
msgstr "Register"

#: templates/mmw_addemar_registration.tpl.php:23
msgid "Please enter your email address"
msgstr "Please enter your email address"

#: templates/mmw_addemar_registration.tpl.php:28
msgid "Please enter a valid email address"
msgstr "Please enter a valid email address"

msgid "register succcess"
msgstr "Thank you for your subscription"

msgid "register error"
msgstr "An error has occured"

